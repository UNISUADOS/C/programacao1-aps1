# UNISUAM #

## Ciência da Computação ##

## APS 1 ##

- **Matéria**: PROGRAMAÇÃO I
- **Professor**: Marcelo Fatudo
- **Período**: 2018.2
- **Tema**: Sistema de Almoxarifado

### Descrição ###

Um sistema de almoxarifado que permite cadastrar **itens** e **usuários** assim
como a entrada e saída de itens (que são realizadas por usuários). O sistema
também exibirá um histórico de transações com o nome de usuário referente a cada
transação.

### Funcionalidades pretendidas ###

As listas abaixo encontram-se em ordem cronológica (ou de importância) de
implementação de cada funcionalidade.

#### Funcionalidades primárias ####

Funcionalidades que **deverão** ser implementadas.

1. Cadastrar **itens** que possuem **código**, **nome** e **quantidade**
2. Cadastrar **entrada** e **saída** de itens já cadastrados
3. Exibir **listagem de itens** cadastrados
4. Cadastrar **usuários** que possuem **nome** e **setor**
5. Guardar transações realizadas por usuários e listá-las

#### Funcionalidades secundárias ####

Funcionalidades que **poderão** ser implementadas apenas quando forem
implementadas todas as funcionalidades primárias e caso haja tempo.

1. Ordenar a **listagem de itens** por **código**, **nome** ou **quantidade** em
   ordem **crescente** ou **decrescente**
2. Filtrar o **histórico de transações** por **usuário** ou **item**
3. Refatorar código para utilizar **struct** para representar **item**,
   **usuário** e **transação**
4. Utilizar arquivos para salvar as informações do programa antes de fechá-lo,
   permitindo recuperá-las ao abrir novamente o programa