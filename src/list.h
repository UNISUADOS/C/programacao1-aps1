#ifndef __LIST_HEADERFILE
#define __LIST_HEADERFILE

typedef void (*FreeFunction)(void*);
typedef int (*Iterator)(void*);

typedef struct _Node {
    void* Data;
    struct _Node* Next;
} Node;

typedef struct _List {
    int Length;
    int ElementSize;

    struct _Node* Head;
    struct _Node* Tail;

    FreeFunction FreeFunction;
} List;

List* List_New(size_t element_size, FreeFunction free_function);
void List_Destroy(List* list);

void List_Append(List* list, void* element);
void List_Prepend(List* list, void* element);

void List_ForEach(List* list, Iterator iterator);

int CompareFloat(float a, float b);
int CompareInt(int a, int b);

typedef enum SortingOrder { ASCENDING = 1, DESCENDING = 2} SortingOrder;
typedef int (*CompareFunction)(void*, void*);
void List_Sort(List* list, SortingOrder order, CompareFunction compare);

void* List_Pop(List* list);

#endif