#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>

// HELPER FUNCTIONS //

int Min(int a, int b)
{
    if(a < b)
    {
        return a;
    }

    return b;
}

int Max(int a, int b)
{
    if(a > b)
    {
        return a;
    }

    return b;
}

void TerminateStringAtNewLine(char* str, size_t length)
{
    for(int i = 0; i < length; i++)
    {
        char current_char = str[i];

        if(current_char == '\0')
        {
            break;
        }

        if(current_char == '\n')
        {
            str[i] = '\0';
            break;
        }
    }    
}

//USER FUNCTIONS
#define USERS_ARRAY_SIZE 20
#define USERS_NAME_LENGTH 100
#define USERS_SECTOR_LENGTH 100

typedef struct User
{
    int Code;
    char Name[USERS_NAME_LENGTH];
    char Sector[USERS_SECTOR_LENGTH];
} User;

User users[USERS_ARRAY_SIZE] = {0};

int GetLastUserIndex()
{
    for(int i = 0; i < USERS_ARRAY_SIZE; i++)
    {
        if(users[i].Code == 0)
        {
            return i-1;
        }
    }

    return USERS_ARRAY_SIZE;
}

int GetUserIndex(int code)
{
    if(code <= 0)
    {
        return -1;
    }

    for(int i = 0; i < USERS_ARRAY_SIZE; i++)
    {
        int current_code = users[i].Code;

        if(current_code == 0)
        {
            return -1;
        }

        if(current_code == code)
        {
            return i;
        }
    }

    return -1;
}

User GetUser(int code)
{
    int index = GetUserIndex(code);

    return users[index];
}

int UserExists(int code)
{
    if(GetUserIndex(code) == -1)
    {
        return 0;
    }
    
    return 1;
}

void AddUser(int code, char* name, char* sector)
{
    int index = GetLastUserIndex() + 1;

    TerminateStringAtNewLine(name, USERS_NAME_LENGTH);
    TerminateStringAtNewLine(sector, USERS_SECTOR_LENGTH);

    users[index].Code = code;
    strcpy(users[index].Name, name);
    strcpy(users[index].Sector, sector);
}

// ITEM FUNCTIONS //
#define ITEMS_ARRAY_SIZE 20
#define ITEMS_NAME_LENGTH 100

typedef struct Item
{
    int Code;
    char Name[ITEMS_NAME_LENGTH];
    int Quantity;
} Item;

Item items[ITEMS_ARRAY_SIZE] = {0};

int GetLastItemIndex()
{
    for(int i = 0; i < ITEMS_ARRAY_SIZE; i++)
    {
        int current_code = items[i].Code;

        if(current_code == 0)
        {
            return i - 1;
        }
    }

    return ITEMS_ARRAY_SIZE;
}

int GetItemIndex(int code)
{
    for(int i = 0; i < ITEMS_ARRAY_SIZE; i++)
    {
        int current_code = items[i].Code;

        if(current_code == 0)
        {
            return -1;
        }

        if(current_code == code)
        {
            return i;
        }
    }

    return -1;
}

Item GetItem(int code)
{
    int index = GetItemIndex(code);
    
    return items[index];
}

int ItemExists(int code)
{
    if(GetItemIndex(code) == -1)
    {
        return 0;
    }

    return 1;
}

void AddItem(int code, char* name, int quantity)
{
    int index = GetLastItemIndex() + 1;

    TerminateStringAtNewLine(name, ITEMS_NAME_LENGTH);

    items[index].Code = code;
    items[index].Quantity = quantity;
    strcpy(items[index].Name, name);
}

void PrintItem(Item item)
{
    printf("%-10s: %d\n", "CODIGO", item.Code);
    printf("%-10s: %s\n", "NOME", item.Name);
    printf("%-10s: %d\n", "QUANTIDADE", item.Quantity);
}

//TRANSACTION FUNCTIONS
#define TRANSACTIONS_ARRAY_SIZE 100

enum TransactionType {ADD, REMOVE};

typedef struct Transaction
{
    int Exists;
    User* User;
    Item* Item;
    int Quantity;
    enum TransactionType Type;
} Transaction;

Transaction transactions[TRANSACTIONS_ARRAY_SIZE] = {0};

int GetLastTransactionIndex()
{
    for(int i = 0; i < TRANSACTIONS_ARRAY_SIZE; i++)
    {
        if(!transactions[i].Exists)
        {
            return i-1;
        }
    }

    return TRANSACTIONS_ARRAY_SIZE;
}

void AddTransaction(int user_code, int item_code, int quantity, enum TransactionType type)
{

    int index = GetLastTransactionIndex() + 1;

    int user_index = GetUserIndex(user_code);
    int item_index = GetItemIndex(item_code);

    transactions[index].User = &users[user_index];
    transactions[index].Item = &items[item_index];
    transactions[index].Quantity = quantity;
    transactions[index].Type = type;
    transactions[index].Exists = 1;
}

// IO HELPERS //
void RepeatCharacter(char character, int count)
{
    for(int i = 0; i < count; i++)
    {
        putchar(character);
    }
}

void PrintTitle(const char* title)
{
    system("cls");
    printf(" --- %s ---\n\n", title);
}

void ClearInputBuffer()
{
    fseek(stdin,0,SEEK_END);
}

int IsEmptyOrWhitespace(const char* str)
{
    int hasAnyNonWhitespace = 0;

    int index = 0;
    while(!hasAnyNonWhitespace)
    {
        char current_char = str[index];

        if(current_char == '\0' || current_char == '\n')
        {
            break;
        }

        if(current_char != ' ')
        {
            hasAnyNonWhitespace = 1;
        }

        index++;
    }

    return !hasAnyNonWhitespace;
}

void PressAnyKeyToReturnToMainMenu()
{
    ClearInputBuffer();
    printf("\n\nPressione qualquer tecla para voltar ao menu principal... ");    
    getchar();
}

// MENU CHOICES //
void AddItemChoice()
{
    char title[] = "CADASTRAR ITEM";
    PrintTitle(title);

    char format[] = "%-10s: ";

    // -- CODE -- //
    int code;
    printf(format, "CODIGO");
    scanf("%d", &code);

    if(code <= 0)
    {
        PrintTitle(title);
        printf("Codigo deve ser um numero maior que zero!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    if(ItemExists(code))
    {
        PrintTitle(title);
        printf("Ja existe um item cadastrado com o codigo '%d'!", code);
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    // -- NAME -- //
    char name[ITEMS_NAME_LENGTH];
    printf(format, "NOME");
    ClearInputBuffer();
    fgets(name, ITEMS_NAME_LENGTH, stdin);
    ClearInputBuffer();

    if(IsEmptyOrWhitespace(name))
    {
        PrintTitle(title);
        printf("Nome nao pode ser vazio ou composto apenas de espacos!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    // -- QUANTITY -- //
    int quantity;
    printf(format, "QUANTIDADE");
    scanf("%d", &quantity);

    if(quantity < 0)
    {
        PrintTitle(title);
        printf("Quantidade deve ser um numero igual ou maior que zero!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    char choice;
    printf("\nCADASTRAR ITEM? [s/n]\nESCOLHA: ");
    ClearInputBuffer();
    scanf("%c", &choice);
    
    if(choice != 's' && choice != 'S')
    {
        return;
    }

    AddItem(code, name, quantity);
}

void ShowItemChoice()
{
    char title[] = "EXIBIR ITEM CADASTRADO";
    PrintTitle(title);

    int code;
    printf("CODIGO DO ITEM: ");
    scanf("%d", &code);

    Item item = GetItem(code);

    if(item.Code == 0)
    {
        PrintTitle(title);
        printf("Nao existe um item cadastrado com o codigo '%d'!", code);
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    PrintTitle(title);
    PrintItem(item);
    PressAnyKeyToReturnToMainMenu();
}

void AddItemStockChoice()
{
    char title[] = "ENTRADA DE ITEM CADASTRADO";
    PrintTitle(title);

    int user_code;
    printf("CODIGO DO FUNCIONARIO: ");
    scanf("%d", &user_code);

    PrintTitle(title);

    User user = GetUser(user_code);

    if(user.Code == 0)
    {
        printf("Nao existe um funcionario com esse codigo!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    printf("FUNCIONARIO: %s - %s\n\n", user.Name, user.Sector);

    int item_code;
    printf("CODIGO DO ITEM: ");
    scanf("%d", &item_code);

    Item item = GetItem(item_code);

    PrintTitle(title);

    if(item.Code == 0)
    {
        printf("Nao existe item cadastrado com o codigo '%d'", item_code);
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    PrintItem(item);

    int added_quantity;
    printf("\nQuantidade a ser adicionada: ");
    scanf("%d", &added_quantity);

    PrintTitle(title);

    if(added_quantity <= 0)
    {
        printf("A quantidade a ser adicionada deve ser um numero maior que zero!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    printf("%-10s: %d\n", "CODIGO", item.Code);
    printf("%-10s: %s\n", "NOME", item.Name);
    printf("%-10s: %d (+%d)\n", "QUANTIDADE", item.Quantity + added_quantity, added_quantity);

    char choice;
    printf("\nCONFIRMAR ENTRADA? [s/n]\nESCOLHA: ");
    ClearInputBuffer();
    scanf("%c", &choice);
    
    if(choice != 's' && choice != 'S')
    {
        return;
    }

    item.Quantity += added_quantity;
    AddTransaction(user_code, item.Code, added_quantity, ADD);
}

void RemoveItemStockChoice()
{
    char title[] = "RETIRADA DE ITEM CADASTRADO";
    PrintTitle(title);

    int user_code;
    printf("CODIGO DO FUNCIONARIO: ");
    scanf("%d", &user_code);

    PrintTitle(title);

    User user = GetUser(user_code);

    if(user.Code == 0)
    {
        printf("Nao existe um funcionario com esse codigo!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    printf("FUNCIONARIO: %s - %s\n\n", user.Name, user.Sector);

    int item_code;
    printf("CODIGO DO ITEM: ");
    scanf("%d", &item_code);

    Item item = GetItem(item_code);

    PrintTitle(title);

    if(item.Code == 0)
    {
        printf("Nao existe item cadastrado com o codigo '%d'", item_code);
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    PrintItem(item);

    int removed_quantity;
    printf("\nQuantidade a ser removida: ");
    scanf("%d", &removed_quantity);

    PrintTitle(title);

    if(removed_quantity <= 0)
    {
        printf("A quantidade a ser retirada deve ser um numero maior que zero!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    if(removed_quantity > item.Quantity)
    {
        printf("A quantidade a ser retirada nao pode exceder a quantidade do item!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    printf("%-10s: %d\n", "CODIGO", item.Code);
    printf("%-10s: %s\n", "NOME", item.Name);
    printf("%-10s: %d (-%d)\n", "QUANTIDADE", item.Quantity - removed_quantity, removed_quantity);

    char choice;
    printf("\nCONFIRMAR RETIRADA? [s/n]\nESCOLHA: ");
    ClearInputBuffer();
    scanf("%c", &choice);
    
    if(choice != 's' && choice != 'S')
    {
        return;
    }

    item.Quantity -= removed_quantity;
    AddTransaction(user_code, item.Code, removed_quantity, REMOVE);
}

void ListItemsChoice()
{
    char title[] = "LISTAR ITENS";
    PrintTitle(title);

    if(items[0].Code == 0)
    {
        printf("Nao ha itens cadastrados!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    //find longest name
    int longest_name = 0;
    for(int i = 0; i < ITEMS_ARRAY_SIZE; i++)
    {
        if(items[i].Code == 0)
        {
            break;
        }

        Item item = items[i];

        int current_name_length = strlen(item.Name);
        if(current_name_length > longest_name)
        {
            longest_name =current_name_length;
        }
    }

    //BOX-BUILDING CHARS:
    const char TOP_LEFT     = '\xc9'; // ╔
    const char BOTTOM_LEFT  = '\xc8'; // ╚
    const char TOP_RIGHT    = '\xbb'; // ╗    
    const char BOTTOM_RIGHT = '\xbc'; // ╝

    const char VERTICAL       = '\xba'; // ║
    const char VERTICAL_LEFT  = '\xb9'; // ╣
    const char VERTICAL_RIGHT = '\xcc'; // ╠
    
    const char HORIZONTAL      = '\xcd'; // ═
    const char HORIZONTAL_DOWN = '\xcb'; // ╦
    const char HORIZONTAL_UP   = '\xca'; // ╩

    const char HORIZONTAL_VERTICAL = '\xce'; // ╬

    //CODIGO = 6 / total = 8
    char code_format[] = " %-6d ";
    int code_header_width = 6 + 2;

    //QUANTIDADE = 10 / total = 12
    char quantity_format[] = " %-10d ";
    int quantity_header_width = 10 + 2;

    //NOME = 4
    int name_spacing = Max(4, longest_name);
    char name_format[] = " %-*s ";
    int name_header_width = name_spacing + 2;

    //PRINT TOP BORDER: ╔════╦════╦════╗    
    putchar(TOP_LEFT);
    RepeatCharacter(HORIZONTAL, code_header_width);
    putchar(HORIZONTAL_DOWN);
    RepeatCharacter(HORIZONTAL, quantity_header_width);
    putchar(HORIZONTAL_DOWN);
    RepeatCharacter(HORIZONTAL, name_header_width);
    putchar(TOP_RIGHT);

    putchar('\n');

    //PRINT HEADERS: ║ CODIGO ║ QUANTIDADE ║ NOME ║
    printf("%c CODIGO %c QUANTIDADE %c %-*s %c\n", VERTICAL, VERTICAL, VERTICAL, name_spacing, "NOME", VERTICAL);

    //PRINT LINE UNDER HEADERS: ╠════╬════╬════╣
    putchar(VERTICAL_RIGHT);
    RepeatCharacter(HORIZONTAL, code_header_width);
    putchar(HORIZONTAL_VERTICAL);
    RepeatCharacter(HORIZONTAL, quantity_header_width);
    putchar(HORIZONTAL_VERTICAL);
    RepeatCharacter(HORIZONTAL, name_header_width);
    putchar(VERTICAL_LEFT);
    putchar('\n');

    //PRINT ITEMS
    for(int i = 0; i < ITEMS_ARRAY_SIZE; i++)
    {
        Item item = items[i];

        if(item.Code == 0)
        {
            break;
        }

        putchar(VERTICAL); // ║
        printf(code_format, item.Code);
        putchar(VERTICAL); // ║
        printf(quantity_format, item.Quantity);
        putchar(VERTICAL); // ║
        printf(name_format, name_spacing, item.Name);
        putchar(VERTICAL);
        
        putchar('\n');
    }

    //PRINT BOTTOM BORDER c8: ╚ / ca: ╩ / cd: ═ / bc: ╝
    putchar(BOTTOM_LEFT); // ╚
    RepeatCharacter(HORIZONTAL, code_header_width); // ═
    putchar(HORIZONTAL_UP); // ╦
    RepeatCharacter(HORIZONTAL, quantity_header_width); // ═
    putchar(HORIZONTAL_UP); // ╦
    RepeatCharacter(HORIZONTAL, name_header_width); // ═
    putchar(BOTTOM_RIGHT); // ╗
    printf("\n\n");

    PressAnyKeyToReturnToMainMenu();
}

void RegisterUserChoice()
{
    char title[] = "CADASTRAR FUNCIONARIO";
    PrintTitle(title);

    char format[] = "%-6s: ";
    
    char name[USERS_NAME_LENGTH];
    printf(format, "NOME");
    ClearInputBuffer();
    fgets(name, USERS_NAME_LENGTH, stdin);
    ClearInputBuffer();

    if(IsEmptyOrWhitespace(name))
    {
        PrintTitle(title);
        printf("Nome nao pode ser vazio ou apenas espacos!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    char sector[USERS_SECTOR_LENGTH];
    printf(format, "SETOR");
    ClearInputBuffer();
    fgets(sector, USERS_SECTOR_LENGTH, stdin);
    ClearInputBuffer();

    if(IsEmptyOrWhitespace(sector))
    {
        PrintTitle(title);
        printf("Setor nao pode ser vazio ou apenas espacos!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    int last_user_index = GetLastUserIndex();
    int code = last_user_index + 2;

    printf(format, "CODIGO");
    printf("%d", code);

    char choice;
    printf("\nCADASTRAR FUNCIONARIO? [s/n]\nESCOLHA: ");
    ClearInputBuffer();
    scanf("%c", &choice);
    
    if(choice != 's' && choice != 'S')
    {
        return;
    }

    AddUser(code, name, sector);
}

void ListUsersChoice()
{
    char title[] = "FUNCIONARIOS CADASTRADOS";
    PrintTitle(title);
    
    if(users[0].Code == 0)
    {
        printf("Nao ha funcionarios cadastrados!");
        PressAnyKeyToReturnToMainMenu();
        return;        
    }

    for(int i = 0; i < USERS_ARRAY_SIZE; i++)
    {
        User user = users[i];
        
        if(user.Code == 0)
        {
            break;
        }

        printf("NOME  : %s\n", user.Name);
        printf("SETOR : %s\n", user.Sector);
        printf("CODIGO: %d\n", user.Code);

        putchar('\n');
    }

    PressAnyKeyToReturnToMainMenu();
    return;
}

void ListTransactionsChoice()
{
    char title[] = "HISTORICO DE TRANSACOES";
    PrintTitle(title);

    if(!transactions[0].Exists)
    {
        printf("Ainda nao ha transacao salva!");
        PressAnyKeyToReturnToMainMenu();
        return;
    }

    for(int i = 0; i < TRANSACTIONS_ARRAY_SIZE; i++)
    {
        Transaction transaction = transactions[i];
        
        if(!transaction.Exists)
        {
            break;
        }

        User user = *transaction.User;
        Item item = *transaction.Item;

        printf("  TRANSACAO: %s\n", transaction.Type == ADD ? "ADICIONOU" : "RETIROU");
        printf("FUNCIONARIO: %s - %s (Cod. %d)\n", user.Name, user.Sector, user.Code);
        printf("       ITEM: %s (Cod. %d)\n", item.Name, item.Code);
        printf(" QUANTIDADE: %d\n", transaction.Quantity);

        putchar('\n');
    }

    PressAnyKeyToReturnToMainMenu();
}

// MENU //
#define MENU_CHOICE_EXIT 0
#define MENU_CHOICE_ADD_ITEM 1
#define MENU_CHOICE_SHOW_ITEM 2
#define MENU_CHOICE_LIST_ITEMS 3
#define MENU_CHOICE_ADD_ITEM_STOCK 4
#define MENU_CHOICE_REMOVE_ITEM_STOCK 5
#define MENU_CHOICE_LIST_TRANSACTIONS 6
#define MENU_CHOICE_REGISTER_USER 7
#define MENU_CHOICE_LIST_USERS 8


int GetUserChoice()
{
    char title[] = "MENU PRINCIPAL";
    PrintTitle(title);

    printf("ITENS:\n");
    printf("[%d] Cadastrar Item\n", MENU_CHOICE_ADD_ITEM);
    printf("[%d] Exibir Item Cadastrado\n", MENU_CHOICE_SHOW_ITEM);
    printf("[%d] Listar Itens Cadastrados\n\n", MENU_CHOICE_LIST_ITEMS);

    printf("TRANSACOES:\n");
    printf("[%d] Entrada de Item Cadastrado\n", MENU_CHOICE_ADD_ITEM_STOCK);
    printf("[%d] Retirada de Item Cadastrado\n", MENU_CHOICE_REMOVE_ITEM_STOCK);
    printf("[%d] Listar Transacoes\n\n", MENU_CHOICE_LIST_TRANSACTIONS);

    printf("FUNCIONARIOS:\n");
    printf("[%d] Cadastrar Funcionario\n", MENU_CHOICE_REGISTER_USER);
    printf("[%d] Listar Funcionarios Cadastrados\n", MENU_CHOICE_LIST_USERS);
    
    printf("\n[%d] Sair\n", MENU_CHOICE_EXIT);
    
    int choice;
    printf("\nESCOLHA: ");
    scanf("%d", &choice);

    return choice;
}

void ShowMainMenu()
{
    int choice = GetUserChoice();

    while(choice != MENU_CHOICE_EXIT)
    {
        system("cls");

        switch(choice)
        {
            case MENU_CHOICE_ADD_ITEM:
                AddItemChoice();
                break;
            case MENU_CHOICE_SHOW_ITEM:
                ShowItemChoice();
                break;
            case MENU_CHOICE_ADD_ITEM_STOCK:
                AddItemStockChoice();
                break;
            case MENU_CHOICE_REMOVE_ITEM_STOCK:
                RemoveItemStockChoice();
                break;
            case MENU_CHOICE_LIST_ITEMS:
                ListItemsChoice();
                break;
            case MENU_CHOICE_REGISTER_USER:
                RegisterUserChoice();
                break;
            case MENU_CHOICE_LIST_USERS:
                ListUsersChoice();
                break;
            case MENU_CHOICE_LIST_TRANSACTIONS:
                ListTransactionsChoice();
                break;
            case MENU_CHOICE_EXIT:
                return;
            default:
                break;
        }

        choice = GetUserChoice();
    }
}

int main()
{
    ShowMainMenu();
    
    system("cls");
    printf("Programa finalizado!\n\n");
    system("pause");

    return 0;
}