#!/bin/sh

SOURCE_FOLDER="src/"
SOURCE_FILES=""
OUTPUT_FILE="dist/Almoxarifado.exe"

for entry in $SOURCE_FOLDER/*.c
do
  SOURCE_FILES+="$SOURCE_FOLDER$(basename $entry)"
done
gcc $SOURCE_FILES -o $OUTPUT_FILE