#!/bin/sh
#COLOR HIGHLIGHTING
error_color="\e[1;31m"
warning_color="\e[1;33m"
normal_color="\e[0m"

error="s/(^error|^.*[^a-z]error:)/$(printf $error_color)\\1$(printf $normal_color)/i"
warning="s/(^warning|^.*[^a-z]warning:)/$(printf $warning_color)\\1$(printf $normal_color)/i"
## ----------------------- ##

SOURCE_FOLDER="src"

OUTPUT_FILE="bin/almoxarifado.exe"

SOURCE_FILES=""

for entry in $SOURCE_FOLDER/*.c 
do
  SOURCE_FILES+="$entry "
done

gcc -ggdb \
    $SOURCE_FILES \
    -o $OUTPUT_FILE \
    2>&1 | sed -ru -e "$warning" -e "$error"