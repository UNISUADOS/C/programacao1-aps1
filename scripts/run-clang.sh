#!/bin/sh

SOURCE_FOLDER="src"
OUTPUT_FOLDER="bin/temp"
OUTPUT_FILE="$OUTPUT_FOLDER/a.out"

for entry in $SOURCE_FOLDER/*.c
do
  
  mkdir $OUTPUT_FOLDER

  clang -O1 -g -fsanitize=address,undefined --analyze $entry -o $OUTPUT_FILE

  ERROR_CODE=$?

  if [ $ERROR_CODE -eq 0 ]
  then
    echo "$entry... OK."
  fi

  echo
  
  rm -r $OUTPUT_FOLDER
done




