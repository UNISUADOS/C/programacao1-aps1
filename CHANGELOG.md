# Changelog #

Todas as mudanças notáveis desse projeto serão documentadas neste arquivo.

O formato desse arquivo é baseado em [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [1.1.0](https://gitlab.com/UNISUADOS/C/programacao1-aps1/tags/1.1.0) ##

### Modificado ###

- [Itens agora são implementados em struct](#18)
- [Funcionários agora são implementados em struct](#19)
- [Transações agora são implementadas em struct](#20)

## [1.0.0](https://gitlab.com/UNISUADOS/C/programacao1-aps1/tags/1.0.0) ##

**Release 1.0.0!**

## [0.5.0](https://gitlab.com/UNISUADOS/C/programacao1-aps1/tags/0.5.0) ##

### Adicionado ###

- [Armazenar histórico de transações](#8)
- [Exibir histórico de transações](#9)

## [0.4.0](https://gitlab.com/UNISUADOS/C/programacao1-aps1/tags/0.4.0) ##

### Adicionado ###

- [Cadastrar funcionários](#6)
- [Exibir funcionários cadastrados](#7)

## [0.3.0](https://gitlab.com/UNISUADOS/C/programacao1-aps1/tags/0.3.0) ##

### Adicionado ###

- [Listagem de itens cadastrados](#5)

## [0.2.0](https://gitlab.com/UNISUADOS/C/programacao1-aps1/tags/0.2.0) ##

### Adicionado ###

- [Entrada e saída de itens cadastrados](#4)

## [0.1.0](https://gitlab.com/UNISUADOS/C/programacao1-aps1/tags/0.1.0) ##

### Adicionado ###

- [Funções para exibição do menu principal](#1)
- [Cadastro de itens com **código**, **nome** e **quantidade**](#2)
- [Exibição de item cadastrado](#3)